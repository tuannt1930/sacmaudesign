var toggle = true;
var menu = document.getElementById('menu').onclick = toggleMenu;
var href = document.getElementById('menu').children;
var navlist = document.getElementById('nav__list');

function toggleMenu() {
    toggle = !toggle;
    if (!toggle) {
        navlist.classList.add('active__list');
    }
    if (toggle) {
        navlist.classList.remove('active__list');
    }
    for (let index = 0; index < href.length; index++) {
        var a = href[index].children;
        for (let i = 0; i < a.length; i++) {
            var element = a[i];
            if (toggle) {
                element.classList.remove('fa-close')
                element.classList.add('fa-navicon')
            } else {
                element.classList.remove('fa-navicon')
                element.classList.add('fa-close')

            }
        }
    }
}