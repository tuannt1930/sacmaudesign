<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>INDEX</title>
    <link rel="stylesheet" href="./assets/style/css/main.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
    <header> 
        <?php include './include/header.php'; ?>
    </header>
    <section id="about">
        <div class="title">
            <h3>Về chúng tôi</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
        </div>
        <div class="about">
            <div class="about__left">
                <img class="left-img" src="./assets/img/about_img.png" alt=""> 
            </div>
            <div class="about__right">
                <h2>Sắc màu design</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Non quam itaque sapiente debitis fuga velit aut veniam tempora neque! Quis ex repellendus culpa accusantium quas fugiat molestias placeat magni explicabo.
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Non quam itaque sapiente debitis fuga velit aut veniam tempora neque! Quis ex repellendus culpa accusantium quas fugiat molestias placeat magni explicabo.
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Non quam itaque sapiente debitis fuga velit aut veniam tempora neque! Quis ex repellendus culpa accusantium quas fugiat molestias placeat magni explicabo.
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Non quam itaque sapiente debitis fuga velit aut veniam tempora neque! Quis ex repellendus culpa accusantium quas fugiat molestias placeat magni explicabo.
                </p>
            </div>
        </div>
       
    </section>
    <script src="./assets/js/allscript.js"></script>
</body>
</html>